package com.company.FigurePackage;

public class Circle extends Figure{
    Circle(double radius){
        this.sides = new double[1];
        this.sides[0] = radius;
    }

    @Override
    double area() {
        return Math.PI * this.sides[0]*this.sides[0];
    }


    @Override
    double calcPerimter() {
        return 2*Math.PI*this.sides[0];
    }

    @Override
    public String toString() {
        return "Радиус: "+sides[0]+".\n"+super.toString();
    }
}

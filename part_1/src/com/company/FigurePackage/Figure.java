package com.company.FigurePackage;

public abstract class Figure {
    double[] sides;

    double calcPerimter(){
        double p = 0;
        for (double side:
             sides) {
            p += side;
        }
        return p;
    }

    double area(){
        double a = 1;
        for (double side:
                sides) {
            a *= side;
        }
        return a;
    }
    @Override
    public String toString() {
        return "Периметр: " + calcPerimter() + ". Площадь: " + area() + ".";
    }
}

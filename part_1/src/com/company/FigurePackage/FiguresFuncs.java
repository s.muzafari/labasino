package com.company.FigurePackage;

import java.util.ArrayList;
import java.util.List;

public class FiguresFuncs {
    public static void main(String[] args) {
        List<Figure> figures = new ArrayList<>();
        figures.add(new Rectangle(10,55));
        figures.add(new Triangle(10,4, 7));
        figures.add(new Circle(9));

        for (var figure:
             figures) {
            System.out.println(figure.toString());
        }
    }
}

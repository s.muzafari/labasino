package com.company.FigurePackage;

public class Rectangle extends Figure{
    Rectangle(double a, double b){
        this.sides = new double[2];
        this.sides[0] = a;
        this.sides[1] = b;
    }

    @Override
    double area() {
        return super.area();
    }

    @Override
    double calcPerimter() {
        return super.calcPerimter();
    }

    @Override
    public String toString() {
        return "a: "+sides[0]+" b: "+sides[1]+".\n"+super.toString();
    }
}

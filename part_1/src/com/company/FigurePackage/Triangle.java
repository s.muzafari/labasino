package com.company.FigurePackage;

public class Triangle extends Figure {
    Triangle(double a, double b, double c) {
        this.sides = new double[3];
        this.sides[0] = a;
        this.sides[1] = b;
        this.sides[2] = c;
    }

    @Override
    double calcPerimter() {
        return super.calcPerimter();
    }

    @Override
    double area() {
        double p = calcPerimter()/2;
        return Math.sqrt(p*(p-sides[0])*(p-sides[1])*(p-sides[2]));
    }
    @Override
    public String toString() {
        return "a: "+sides[0]+" b: "+sides[1]+" c: "+sides[2]+".\n"+super.toString();
    }
}

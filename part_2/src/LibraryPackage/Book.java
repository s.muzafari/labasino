package LibraryPackage;

public class Book {
public String title;
public String authtor;
public String publisher;
public int publishYear;

public Book(String title, String authtor, String publisher, int publishYear){
    this.title = title;
    this.authtor = authtor;
    this.publisher = publisher;
    this.publishYear = publishYear;
}

    @Override
    public String toString() {
        return this.title + " " + this.authtor + " " + this.publisher + " "+this.publishYear+"\n";
    }
}

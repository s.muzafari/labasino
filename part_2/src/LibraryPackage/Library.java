package LibraryPackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Library {
    public static void main(String[] args) {
                List<Book> books = new ArrayList<>();
                books.add(new Book("Война и мир","Лев Николаевич Толстой", "1С Паблишинг", 2010));
                books.add(new Book("Преступление и наказание","Федор Михайлович Достоевкий", "1С Паблишинг", 2010));
                Scanner scanner = new Scanner(System.in);
                while (true) {
                    System.out.println("1.Поиск по автору");
                    System.out.println("2.Поиск по названию");
                    System.out.println("3.Список");
                    System.out.println("+.Добавление книги");
                    System.out.println("-.Удаление книги");
                    String inp = scanner.next();
                    switch (inp){
                case "1":
                    System.out.println("Поиск по автору");
                    String val = GetInp("Автор книги: ");
                    for(int i = 0; i < books.size(); i++){
                        if(books.get(i).authtor.contains(val)){
                            System.out.println(books.get(i).toString());
                        }
                    }
                    break;
                case "2":
                    System.out.println("Поиск по названию");
                    String valTitle = GetInp("Название книги: ");
                    for(int i = 0; i < books.size(); i++){
                        if(books.get(i).authtor.contains(valTitle)){
                            System.out.println(books.get(i).toString());
                        }
                    }
                    break;
                case "3":
                    System.out.println("Список");
                    for(int i = 0; i < books.size(); i++){
                            System.out.println(books.get(i).toString());
                    }
                    break;
                case "+":
                    System.out.println("Добавление книги");
                    books.add(new Book(
                            GetInp("Название"),
                            GetInp("Автор"),
                            GetInp("Издательство"),
                            Integer.parseInt(GetInp("Год издания"))
                            ));
                    break;
                case "-":
                    System.out.println("Удаление книги");
                    for(int i = 0; i < books.size(); i++){
                        System.out.println(i + books.get(i).toString());
                    }
                    books.remove(Integer.parseInt(GetInp("Номер удаляемой книги")));
                    break;
            }
            if(inp.equals("`"))
                break;
        }
    }
    static String GetInp(String str){
       Scanner scanner = new Scanner(System.in);
        System.out.println(str);
       return scanner.nextLine();
    }
}
